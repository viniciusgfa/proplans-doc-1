
# Installation on windows with WSL2
Installation with windows subsystem scheme using **Ubuntu 20.04 LTS**

## Step 1 - Installing the subsystem
>Install **Ubuntu 20.04 LTS** from **Microsoft Store**
>- Access [https://www.microsoft.com/en-us/p/ubuntu-2004-lts/9n6svws3rx71](https://www.microsoft.com/en-us/p/ubuntu-2004-lts/9n6svws3rx71)
>
>**or**
>
>- Search for Ubuntu 20.04 LTS on the **Microsoft Store** app
>
>![Ubuntu on Microsoft Store](../img/tutorial/windows/wsl2/ubuntu-on-microsoft-store.png)

## Step 2 - Activating required features
>***At the end of this step, restart your computer.**
>
>Access the **Windows Feature** manager and keep the following features enabled:
>
>- [x] Virtual Machine Platform
>- [x] Windows Subsystem for Linux
>
>![Activating required features](../img/tutorial/windows/wsl2/required-features.png)

## Step 3 - Preparing Docker
>1. Access [https://www.docker.com/products/docker-desktop](https://www.docker.com/products/docker-desktop) to download the Docker Desktop installer.
>
>2. Follow the usual installation instructions to **install Docker** Desktop.
>If you are running a supported system, Docker Desktop prompts you to enable WSL 2 during installation. Read the information displayed on the screen and enable WSL 2 to continue.
>
>3. **Start Docker** Desktop from the Windows Start menu.
>From the Docker menu, select **Settings > General**.
>Select the **Use WSL 2 based engine** check box then click on **Apply & Restart**.
>
>![WSL2 Enable](../img/tutorial/windows/wsl2/wsl2-enable.png)
>
>4. Ensure the distribution runs in WSL 2 mode. WSL can run distributions in both v1 or v2 mode.
>- To check the WSL mode, run:
>```
> wsl -l -v
>```
>- To upgrade your existing Linux distro to v2, run:
>```
> wsl --set-version (distro name) 2
>```
>- The Docker-WSL integration will be enabled on your default WSL distribution. To change your default WSL distro, run:
>```
> wsl --set-default <distro name>
>```
>
>5. When Docker Desktop restarts, go to **Settings>Resources>WSL Integration** and enable Ubuntu.
>
>![Ubuntu Enable](../img/tutorial/windows/wsl2/ubuntu-enable.png)

## Step 4 - Installing NodeJs
>1. Open a new terminal as an **administrator** and run the following command to access Ubuntu:
>```
>  ubuntu2004.exe
>```
>
>2. And now, inside Ubuntu, run the following commands to **install NodeJS**:
>```
>  curl -fsSL https://deb.nodesource.com/setup_15.x | sudo -E bash -
>```
>```
>  sudo apt-get install -y nodejs
>```
>
## Step 5 - Cloning the project
>Still inside the terminal running ubuntu, type the git command to clone the repository.
>```
>  git clone https://luidyvcc@bitbucket.org/tutorials/markdowndemo.git
>```
## Step 6 - Generating the container
>Still inside the terminal running ubuntu, type the npm command to generate the Docker container.
>```
>  npm run docker.build
>```
