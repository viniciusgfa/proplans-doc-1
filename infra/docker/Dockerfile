FROM debian:stretch

WORKDIR /var/www/html

ADD . /var/www/html

ENV PATH $PATH:/usr/sbin:/usr/local/nginx/sbin

RUN apt-get update && \
    apt-get --no-install-recommends --no-install-suggests --yes --quiet install \
        apt-transport-https bash-completion lsb-release ca-certificates curl git gnupg imagemagick \
        less make mysql-client perceptualdiff procps ssh-client sudo unzip vim wget libpng-dev && \
    apt-get clean && apt-get --yes --quiet autoremove --purge && \
    rm -rf  /var/lib/apt/lists/* /tmp/* /var/tmp/* \
            /usr/share/doc/* /usr/share/groff/* /usr/share/info/* /usr/share/linda/* \
            /usr/share/lintian/* /usr/share/locale/* /usr/share/man/*

RUN sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
RUN echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list

RUN apt-get update && \
    apt-get --no-install-recommends --no-install-suggests --yes --quiet install \
        php7.3-dev php7.3-fpm php7.3-cli php7.3-apcu php7.3-mbstring php7.3-curl php7.3-gd php7.3-imagick php7.3-intl php7.3-bcmath \
        php7.3-mysql php7.3-xdebug php7.3-xml php7.3-zip php7.3-ldap php7.3-sockets \
        gettext-base php7.3-soap

RUN service php7.3-fpm start

RUN wget https://nginx.org/keys/nginx_signing.key
RUN apt-key add nginx_signing.key

RUN apt-get update && apt-get install -y nginx
RUN curl -sL https://deb.nodesource.com/setup_12.x  | bash -
RUN apt-get -y install nodejs
#RUN npm install
RUN npm install yarn -g
#RUN yarn add react-icons

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && \
    mv composer.phar /usr/local/bin/composer && \
    chmod +x /usr/local/bin/composer

RUN apt-get install php-pear
RUN pecl channel-update pecl.php.net
RUN pecl install xdebug-2.9.8

RUN apt-get install -y apt-transport-https lsb-release ca-certificates
RUN wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
RUN apt-get update
RUN apt-get install -y poppler-utils pdftk

RUN > /etc/php/7.3/mods-available/xdebug.ini
RUN echo "zend_extension=/usr/lib/php/20180731/xdebug.so" >> /etc/php/7.3/mods-available/xdebug.ini
RUN echo "xdebug.remote_enable=1" >> /etc/php/7.3/mods-available/xdebug.ini
RUN echo "xdebug.remote_port=9000" >> /etc/php/7.3/mods-available/xdebug.ini
RUN echo "xdebug.remote_handler=dbgp" >> /etc/php/7.3/mods-available/xdebug.ini
RUN echo "xdebug.remote_autostart=1" >> /etc/php/7.3/mods-available/xdebug.ini
RUN echo "xdebug.remote_connect_back=1" >> /etc/php/7.3/mods-available/xdebug.ini
RUN echo "xdebug.idekey=PHPSTORM" >> /etc/php/7.3/mods-available/xdebug.ini
RUN echo "xdebug.remote_log=/tmp/xdebug.log" >> /etc/php/7.3/mods-available/xdebug.ini
RUN echo "xdebug.remote_mode = req" >> /etc/php/7.3/mods-available/xdebug.ini
RUN echo "xdebug.remote_host = host.docker.internal" >> /etc/php/7.3/mods-available/xdebug.ini 

COPY php-fpm.conf /etc/php-fpm.conf
COPY nginx.conf /etc/nginx/nginx.conf 
COPY php.ini /etc/php/7.3/fpm/php.ini
COPY php.ini /etc/php/7.3/cli/php.ini

VOLUME ["/var/run/php"]

RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www -p www
RUN usermod -aG sudo www

# Copy existing application directory permissions
COPY --chown=www:www . /var/www/html

# Change current user to www
USER www


EXPOSE 9000



